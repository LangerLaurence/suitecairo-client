const { app, BrowserWindow } = require('electron');
const path = require('path');
const { env } = require('./container/environments');
const {initialize, enable } = require('@electron/remote/main');
// Initialisation du module Remote pour pouvoir y accéder depuis les pages web
initialize();

/**
 * Création de la fenêtre principale de l'application
 */
const createWindow = () => {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    icon: __dirname + '/webview/assets/img/logo.ico',
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    },
    autoHideMenuBar: true
  })
  setProxy(win);
  
  enable(win.webContents);
  win.loadFile('webview/index.html')
}

/**
 * Definition d'un proxy
 * @param {*} session brower courant
 */
const setProxy = (session) => {
  let proxy = env.get("proxy");
  if(proxy){
    session.setProxy(proxy);
  }
}

app.whenReady().then(() => {
  createWindow()
})

