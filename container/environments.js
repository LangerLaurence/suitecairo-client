const path = require('path');
const PropertiesReader = require('properties-reader');
const rootPath = require('electron-root-path').rootPath;

//Chargement des propriété de l'application depuis le fichier application.properties
const prop = new PropertiesReader(path.join(rootPath, 'application.properties'));

//On ajoute dans la configuration tous les arguments ayant servis à lancer l'application
process.argv.forEach(function (val, index, array) {
    var arg = val.split("=");
    if (arg.length > 0) {
        prop.set(arg[0], arg[1]);
    }
});

exports.env = prop;