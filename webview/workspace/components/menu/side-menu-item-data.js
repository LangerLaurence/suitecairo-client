import { LitElement, html, css } from '../../../lib/lit-element.js';
import SideMenuItem from './side-menu-item.js';

const { env }  = require('./../../container/environments.js');


class SideMenuItemData extends SideMenuItem {

    /**
     * rajouter un quantille
     */
    addQuantile(){

        //calcule du nouvau range
        let ranges = [...this._layer.slider.ranges];
        let maxValue =  this._layer.slider.maxValue;
        let size = this._layer.slider.ranges.length;
        let lastValue = size > 0 ? this._layer.slider.ranges[size-1]: 0;
        let newRang = lastValue + (maxValue - lastValue)/2;

        //rajouter un range
        ranges.push(newRang);
        this.onUdpateProperty("properties.slider.ranges", ranges);

        //rajouter une nouvelle couleur
        let rangeColors = [...this._layer.style.rangeFillColors];
        rangeColors.push("#000000");
        this.onUdpateProperty("style.rangeFillColors", rangeColors);
        

        this.requestUpdate();
    }

    /**
     * suprimmer un quantille
     */
    removeQuantile(){
        
        let rangeColors = [...this._layer.style.rangeFillColors];
        let ranges = [...this._layer.slider.ranges];
        
        //suprimmer un range
        ranges.pop();
        this.onUdpateProperty("properties.slider.ranges",  ranges);
        
        //suprimmer une  couleur
        rangeColors.pop();
        this.onUdpateProperty("style.rangeFillColors", rangeColors);

        this.requestUpdate();
    }

    /** recuperer le set des couleur depuis le fichhier application.properties */
    colorPalettes(){
        let tab = env.get('colorSets.sets').split(",");

        let title = '';
        let colorSets = {};
        for (let i = 0; i < tab.length; i++) {
            if( !tab[i].startsWith('#')){
                title = tab[i];
                colorSets[title] = [];
                continue;
            }
            colorSets[title].push(tab[i]);
        }
        return colorSets;
    }

    /**
     * appliquer un set de couleur/quantille sur une couche 
     * @param {*} colors 
     */
    setColorSet(colors){
        let ranges = [...this._layer.slider.ranges];
        let rangeColors = [...this._layer.style.rangeFillColors];

        //difference de seuille entre l'ancien et le nouveaux.
        let diff = rangeColors.length - colors.length;
        if(diff>0){
            //enlever des seuille
            for (let i = 0; i < diff; i++) {
                ranges.pop();
            }
        }else if (diff<0){
            //rajouter des seuille
            let maxValue = this._layer.slider.maxValue;
            let pas;
            let value;
            if(this._layer.slider.ranges.length > 0){
                let lastIndex = this._layer.slider.ranges.length - 1 ;
                pas = (maxValue - ranges[lastIndex]) / (Math.abs(diff));
                value = ranges[lastIndex];

            } else{
                // si 0 range c-a-d une seule quantile
                let minValue = this._layer.slider.minValue;

                pas = (maxValue - minValue) / (Math.abs(diff)+1);
                value = minValue;
            }

            // rajouter les ranges manquant
            for (let i = 0; i < Math.abs(diff); i++) {
                value += pas;
                ranges.push(value);
            }
        }
        
        //reinit les ranges
        this.onUdpateProperty("properties.slider.ranges",  ranges);

        //mettre à jour les couleur
        this.onUdpateProperty("style.rangeFillColors", colors);

        this.requestUpdate();
    }

    disableMinusQuantile(){
        return this._layer.slider.ranges == 0 ;
    }

    iconTypeChange(newIconType){
        this.onUdpateProperty("style.iconType", newIconType);
    }

    render() {
            let colorPickers = [];
            for (let i = 0; i < this._layer.style.rangeFillColors.length; i++) {
                const color = this._layer.style.rangeFillColors[i];
                colorPickers.push(this.renderProperty("Couleur de remplissage "+ parseInt(i+1), color, "color", "style.rangeFillColors["+ i +"]"));
            }

            return this._layer ? [this.renderProperty('Titre de la couche', this._layer.title, 'text', "properties.title")
                ,this.renderProperty("Type d'icone", this._layer.style.iconType, "list", "style.iconType")
                ,this.renderTitle("style")
                ,this.renderProperty("Edition de Quantile",  this.rangeFillColors ,"quantile-edit"),
                ,this.renderProperty("palette de couleur",  null ,"color-palettes"),
                , ... colorPickers
            ] : html``;
        
    }
}

customElements.define("workspace-sidemenu-item-data", SideMenuItemData);
export default SideMenuItemData;