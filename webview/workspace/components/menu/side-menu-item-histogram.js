import {html } from '../../../lib/lit-element.js';
import SideMenuItem from "./side-menu-item.js";

class SideMenuItemHistogram extends SideMenuItem{

    render() {
        return this._layer ? [
        this.renderProperty('Titre de la couche', this._layer.title, 'text', "properties.title")
        ] : html``;
    }
}

customElements.define("workspace-sidemenu-item-histogram", SideMenuItemHistogram);
export default SideMenuItemHistogram;