import { LitElement, html, css } from '../../../lib/lit-element.js';
import SideMenuItem from './side-menu-item.js';

class SideMenuItemBackground extends SideMenuItem{

    /**
     * Emission d'une modification de propriété
     * @param {*} associatedProperty nom de la propriété
     * @param {*} value nouvelle valeur de la propriété
     */
    onChange(associatedProperty, value){
        let event = new CustomEvent('update-layer',{ detail: { layer : "background",  property : associatedProperty, value: value}});
        this.dispatchEvent(event);
    }    

    updated(changedProperties){
        if(this._layer){
            this.shadowRoot.getElementById("bgBgColor").value = this._layer.style.bgColor;
            this.shadowRoot.getElementById("bgFillColor").value = this._layer.style.fillColor;
            this.shadowRoot.getElementById("bgBorderColor").value = this._layer.style.borderColor;
        }
    }

    render(){
        return this._layer ? [this.renderTitle("style")
            , this.renderProperty("Couleur de fond",this._layer.style.bgColor,"color","style.bgColor", "bgBgColor")
            , this.renderProperty("Couleur de remplissage",this._layer.style.fillColor,"color","style.fillColor", "bgFillColor")
            , this.renderProperty("Couleur de bordure",this._layer.style.borderColor,"color","style.borderColor", "bgBorderColor")
        ]:html``;
    }
}

customElements.define("workspace-sidemenu-item-background", SideMenuItemBackground);
export default SideMenuItemBackground;