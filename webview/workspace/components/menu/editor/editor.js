import workspacesService from "../../../../shared/service/workspacesService.js";
/**
 * Quill ne fonctionne pas à l'interieur d'un web component donc on utilise un div 
 * que l'on a définit en light dom au niveau de la page et on initialize quill ici
 */

 const Embed = Quill.import('blots/embed');
 const SAVE_INTERVALL = 5000;

/**
 * Utilitaire permettant de retourver l'instance de quill associé à un noeud du dom
 * @param {*} node 
 * @returns 
 */
 function findQuill(node) {
    while (node) {
      const quill = Quill.find(node);
      if (quill instanceof Quill) return quill;
      node = node.parentElement;
    }
  }

 /**
  * Lien d'affichage vers un état de la carte :
  * - double click => on affiche l'état
  * - click droit => on supprime le lien
  */
 class StateBlot extends Embed{
     /**
      * Création de la balise
      * @param {*} value 
      * @returns 
      */
    static create(value) {
        let node = super.create()
    
        node.setAttribute('href', '#');
        node.setAttribute('data-id', value.id);
        node.setAttribute('target', '_blank');
        node.innerText = value.text
        
        let restoreState = (e) => {
            e.preventDefault();
            const quill = findQuill(node);
            if(!quill.isEnable()){
                let event = new CustomEvent('restore-state',{ detail: { id : value.id, title : value.text}});
                window.dispatchEvent(event);
            }
            return false;
        };
        /*node.addEventListener('contextmenu',(e) => {
            e.preventDefault();
            const quill = findQuill(node);
            if(quill.isEnable()){
                node["__blot"].blot.remove();
            }
            return false;
        });*/ 
        node.onclick = restoreState;
        return node  
    }
    
    /**
     * Récupération de la valeur associé à la balise
     * @param {*} node 
     * @returns 
     */
    static value(node) {
        return { id : node.getAttribute('data-id'), text: node.innerText }
    }
    
    /**
     * Récupération de la valeur de l'attribut href
     * @param {*} node 
     * @returns 
     */
    static formats(node) {
        return node.getAttribute('href')
    }

    /**
     * Suppression de la balise.
     * On nettoie la sauvegarde de l'état associé
     */
    remove(){
        super.remove();
        const backupId = this.domNode.getAttribute("data-id");
        workspacesService.deleteBackupWorkspace(null, backupId);
    }
}
StateBlot.blotName = 'state'
StateBlot.tagName = 'SPAN'
StateBlot.className = 'state'

//déclaration du nouveau Blot pour la sauvegarde de l'état
Quill.register(StateBlot);

class Editor extends HTMLElement{
    _quill;
    _save;
    _lock;
    _isEnable;

    /**
     * Intiialisation de l'édituer WYSIWYG Quill
     */
    constructor() {
        super();
        this._save = true;
        this._isEnable = false;
        this._quill = new Quill("#editor", {
            modules: {
                toolbar : '#toolbar'
            },
            theme: 'snow'
        });

        this._quill.enable(this._isEnable);
        this._quill.isEnable = () => {
            return !this._quill.container.classList.contains('ql-disabled');
        }
        this._extendTooltip();
        
        //définition des actions des nouveaux boutons
        document.getElementById("savestate").addEventListener("click", this.displayStateDefinitionPopup.bind(this));
        this._lock = document.getElementById("togglelock");
        this._lock.addEventListener("click", this._toogleLock.bind(this));

        //chargement du contenu de l'éditeur
        workspacesService.loadWorkspaceScenario().then((delta) => this._quill.setContents(delta));

        this._quill.on('text-change', this.save.bind(this));

        //gestion de l'affichage en fonction du fait que nous affichons l'état courant ou un état passé dans la carte
        window.addEventListener("restore-state", (e) => { 
            if(e.detail.id){
                this._quill.enable(false);
                this._lock.style.display = "none";
            }else{
                this._quill.enable(this._isEnable);
                this._lock.style.display = "block";
            }
        });
    }

    /**
     * 
     * @param {*} delta 
     */
    save(delta){
        if(this._save){
            this._save = false;
            workspacesService.saveWorkspaceScenario(null, this._quill.getContents());
            setTimeout((() => { this._save = true;}).bind(this), SAVE_INTERVALL);
        }
    }

    /** passage du mode lecture en mode écriture */
    _toogleLock(){
        this._isEnable = !this._isEnable;
        this._quill.enable(this._isEnable);
        this._lock.className = this._isEnable ? "unlock" : "lock";
    }
    /**
     * On étant le tooltip standard pour ajouter un spécifique pour la sauvegarde de l'environnement
     */
    _extendTooltip(){
        const tooltip = this._quill.theme.tooltip;
        //recalcul de la position
        const position = tooltip.position;
        tooltip.position = ((ref) => { position.call(tooltip,ref); tooltip.root.style.left = "0px";});

        //définition de la sauvegarde
        const save = tooltip.save;
        tooltip.save = (() => { 
            if(tooltip.root.getAttribute('data-mode') === "backup"){
                const id = this.saveState();
                const range = this._quill.getSelection(true);
                const index = (range != null) ? range.index + range.length : 0;
                const text = tooltip.textbox.value;
                this._quill.insertEmbed(index, "state", {id : id, text: text}, "user");
                this._quill.setSelection(index + text.length)
                tooltip.hide();
            }else{
                save.call(tooltip)
            }
        });
        
    }

    /**
     * Affichage du tooltip pour la sauvgarder d'état
     */
    displayStateDefinitionPopup(){
        const tooltip = this._quill.theme.tooltip;
        tooltip.edit('backup', "state");
    }

    /**
     * Sauvegarde état
     */
    saveState(){
        return workspacePage().backup();
    }
}

customElements.define("scenario-editor", Editor);