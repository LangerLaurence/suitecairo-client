import { LitElement, html, css } from '../../../lib/lit-element.js';
import workspacesService from '../../../shared/service/workspacesService.js';

const FORMAT = {year : "numeric", month : "numeric" , day : "numeric", hour:"numeric", minute:"numeric"};
class ServerWorkspace extends LitElement{

    /** liste des workspaces sur le server */
    serverWorkspaces = [];

    /**
     * Constructeur chargement de la liste des projets recents
     */
    constructor() {
        super();
        this._loadData();
    }
  
  
      /**
       * Récupère la liste des cartes sur le serveur puis en local
       */
      _loadData() {
          workspacesService.getWorkspacesList((data) => {      
            this.serverWorkspaces = data;
            this.requestUpdate();
          }, (msg) => {
            console.error("Impossible de récupérer les workspaces sur le serveur : " + msg);
            this.serverWorkspaces = [];
          })
      }

    //affichage de la liste
    render(){
        return html`${this.serverWorkspaces.map((element, i) => 
                html`
                <div class="card1" @click="${(e) => this._launchWorkspace(element)}">
                    <div class="title">${element.title}</div>
                    <div class="property">
                        <div class="property-label">Echelle</div>
                        <div class="property-value">${element.echelle}</div>
                    </div>
                    <div class="property">
                        <div class="property-label">Maille</div>
                        <div class="property-value">${element.maille}</div>
                    </div>
                </div>
                `)}`;
    }

    static get styles()
    {
        return css`
        :host{
            display: flex;
            flex-direction: row;
            width : 100%;
            flex-wrap: wrap;
        }

        div.card1 {
            margin : 20px;
            cursor: pointer;
            transition: all 0.3s cubic-bezier(0.17, 0.67, 0.35, 0.95);
            height : 100px;
            width:400px;
            padding : 20px;
            justify-content: center;
            align-items: center;
            text-align: center;
            opacity: 0.8;
            border : var(--border-width1) solid var(--border-color1);
            font-size: var(--font-size-header2);
            background-color: var(--bg-color-alt5);
            color : var(--color-alt2);
        }

        div.card1:hover {
            border-color : transparent;
        }

        div.title{
            width:100%;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            width: 100%;
        }

        div.property{
            width:100%;
            display : flex;
        }

        div.property-label{
            font-size: var(--font-size-body1);
            text-align: left;
            font-weight : bold;
            width : 105px;
        }

        div.property-value{
            font-size: var(--font-size-body1);
            text-align: left;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
        }
        `;
    }
}

customElements.define("server-workspace", ServerWorkspace);
export default ServerWorkspace;