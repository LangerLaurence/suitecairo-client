import { LitElement, html, css } from '../../../lib/lit-element.js';
import { loadJson} from '../../../shared/util/file.js';

import "../../../shared/component/icon/faicon.js";
import "../../../shared/component/icon/fabutton.js";

import workspacesService from "../../../shared/service/workspacesService.js";
import IconTypeConstants from '../../../shared/util/IconTypeConstants.js';
import layerService from '../../../shared/service/layerService.js';
const fs = require('fs');
const path = require('path');
const { env } = require('../../container/environments.js');

const workspace = env.get("workspace.location");


let expanded = false;
class LayerCreation extends LitElement{
    /** tableau des calculs*/
    calculTypeTab = ["valeur a","somme","(somme/d)%","(a/b)%","[(a-b)/c]%","[S1/S2]%","[(a-b)/S]%"];
    /** tableau des types d'icones */
    codeTypeTab = ["2,0","2,4","2,3","2,1","2,2","2,5","2,6"];

    _iconFillColors = ['#93065f'];

    _iconType = 'circle';
    
    _histogramColones = [];

    _histogramTitle;

    _histogramDataSource;

    _histogramDescription;

    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
            workspaceName: { attribute: true, type: String },
            calculType: {attribute: false, reflect : true, type: String},
            codeType: {attribute: false, type: String},
            dataSet: {attribute: false, type: Array},
            layerTitle: {attribute: false, type: String},
            dataSource: {attribute: false, type: String},
            layerDescription : {attribute: false, type: String},
            data1 : {attribute: false, type: Array},
            data2 : {attribute: false, type: String},
            data3 : {attribute: false, type: Array}
        };
    }

    constructor() {
        super();
        this.workspaceName = workspacesService.queryWorkspaceName();
        this.dataSetName = workspacesService.queryDataSetName();
        this.calculType = "";
        this.codeType = "";
        this.dataSet = [];
        this.data1 = [];
        this.data3 = [];
    }

    /**
     * Identification du workspace et chargement des données associées
     * @param {*} changedProperties proporiété modifiéess
     */
    firstUpdated(changedProperties){
        this._loadData();
        this._updateColorPickers();
    }

    /**
     * 
     * @returns charge la liste des données à calculer
     */
    _loadData() {
        const datas = JSON.parse(fs.readFileSync(path.resolve(env.get("workspace.location"), this.workspaceName, "dataSets", this.dataSetName,"data.json")));
        for(const item in datas.data) {
            if (!['idcarte', 'libel', 'codeINSEE'].includes(item))
                this.dataSet.push(item);
        }
        this.requestUpdate();
        return this.dataSet;
    }
    
    /**
     * Récupérer et valider les données insérer dans le formulaire de création de couche
     * @returns true si les données sont valide
     */
    _isLayerCreationFormDataValid(){
        //validate calculation type
        let data1=[];
        let data2=[];
        let data3=[];

        let errorMessageList = [];

        if(this.calculType){
            //validation de la selection de donnée
            
            //only list with multiple choices need to be validated select have the first value as default

            switch (this.calculType) {
                case "valeur a":
                    // no validation needed list with the first element as default
                    data1.push(this.shadowRoot.getElementById('firstNumerator-select').value);
                    break;
                case "somme":
                    //get checked elements
                    this.shadowRoot.querySelectorAll(".firstNumerator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data1.push(element.value);
                        }
                    });

                    //validation : au moin 2 de donnée selectioner
                    if(data1.length < 2){
                        errorMessageList.push("Veuillez selectionner au moin 2 donnée pour le Numérateur 1.")
                    }
                    break;
                case "(somme/d)%":
                     //firstNumerator
                     this.shadowRoot.querySelectorAll(".firstNumerator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data1.push(element.value);
                        }
                    });

                    //validation : au moin 2 de donnée selectioner
                    if(data1.length < 2){
                        errorMessageList.push("sélectionner aux moins 2 données pour le Numérateur 1.")
                    }

                    //denominateur
                    data3.push(this.shadowRoot.getElementById('denominator-select').value);

                    break;
                case "(a/b)%":
                    //firstNumerator
                    data1.push(this.shadowRoot.getElementById('firstNumerator-select').value);

                    //denominateur
                    data3.push(this.shadowRoot.getElementById('denominator-select').value);
                    break;

                case "[(a-b)/c]%":
                    //firstNumerator
                    data1.push(this.shadowRoot.getElementById('firstNumerator-select').value);

                    //secondNumerator
                    data2.push(this.shadowRoot.getElementById('secondNumerator-select').value);

                    //denominateur
                    data3.push(this.shadowRoot.getElementById('denominator-select').value);
                    break;
                case "[S1/S2]%":
                    //firstNumerator
                    this.shadowRoot.querySelectorAll(".firstNumerator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data1.push(element.value);
                        }
                    });

                    //validation firstNumerator: au moin 2 de donnée selectioner
                    if(data1.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Numérateur 1.")
                    }

                    //denominateur
                    this.shadowRoot.querySelectorAll(".denominator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data3.push(element.value);
                        }
                    });

                    //validation denominateur: au moin 2 de donnée selectioner
                    if(data3.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Dénominateur.")
                    }
                    break;
                case "[(a-b)/S]%":
                    //firstNumerator
                    data1.push(this.shadowRoot.getElementById('firstNumerator-select').value);

                    //secondNumerator
                    data2.push(this.shadowRoot.getElementById('secondNumerator-select').value);

                    //denominateur
                    this.shadowRoot.querySelectorAll(".denominator-Checkbox").forEach( (element) => {
                        if(element.checked){
                            data3.push(element.value);
                        }
                    });

                    //validation : au moin 2 de donnée selectioner
                    if(data3.length < 2){
                        errorMessageList.push("Veuillez sélectionner aux moins 2 données pour le Dénominateur.")
                    }
                    break;
            }
            


        }else{
            errorMessageList.push("Veuillez choisir un type de calcul");
        }

        //validation input titre de la couche
        let layerTitle = this.shadowRoot.getElementById('layerTitle').value;

        if(!layerTitle){
            errorMessageList.push("Veuillez rensigner le titre la couche");
        }

        //recueprer la source de donnée
        let dataSource = this.shadowRoot.getElementById('dataSource').value;

        //recueprer le liblele long
        let layerDescription = this.shadowRoot.getElementById('layerDescription').value;

        //recueprer le liblele long
        let iconType = this.shadowRoot.getElementById('icone').value;

        //  envoyer les message d'erreur
        if(errorMessageList.length != 0){
            let str = "<ul>";
            for (let i = 0; i < errorMessageList.length; i++) {
                str += "<li>"+ errorMessageList[i] + "</li>";
            }
            str += "</ul>";
            this.shadowRoot.getElementById('validation-message').innerHTML = str;

            return false;
        }else{
            // affectation des données
            this.data1 = data1;
            this.data2 = data2;
            this.data3 = data3;
            this.layerTitle = layerTitle;
            this.dataSource = dataSource;
            this.layerDescription = layerDescription;
            this._iconType = iconType;
            return true;
        }

    }

    /**
     * création d'une couche donnée 
     */
    _createLayerInfo() {

        let formValidation = this._isLayerCreationFormDataValid();
        
        if(formValidation){
            //correction du titre pour l'utiliser en developpement
            const regexp = /[^A-zÀ-ÿ0-9]/g;
            let name = this.layerTitle.replaceAll(regexp, "").toLowerCase();

            // ecriture des infos de la couche
            let jsonLayer =
                {
                    type: "data",
                    properties: {
                        "title": this.layerTitle,
                        "id": name,
                        "dataSetFile": "./dataSets/"+ this.dataSetName + "/data.json",
                        "calculationType": this.calculType,
                        "usedData": {
                            "firstNumerator": this.data1,
                            "secondNumerator": this.data2,
                            "denominator": this.data3,
                        },
                        "dataSource": this.dataSource,
                        "longTitle": this.layerDescription,
                        "slider": {
                        }
                    },
                    style: {
                        "iconType": this._iconType, //enum : zone, circle, shape, triangle
                        "fillColor": "#F2F2F2", // couleur de remplissage
                        "borderColor": "#FFFFF", // une seule couleur de contour
                        "display": false,
                        "rangeFillColors": this._iconFillColors,
                    }
                }


            this._writeLayerToWorkspaceJson(jsonLayer);

        }

    }

    /**
     * Récupérer et valider les données insérer dans le formulaire de création d'histogramme
     * @returns true si les données sont valide
     */
    _isHistogramCreationFormDataValid(){
        let histogramColones =[];
        let errorMessageList =[]

        //Récupérer les colonnes sélectionnées
        this.shadowRoot.querySelectorAll(".histogramColones-Checkbox").forEach( (element) => {
            if(element.checked){
                histogramColones.push(element.value);
            }
        });

        //validation : au moin 2 de donnée selectioner
        if(histogramColones.length == 0){
            errorMessageList.push("Veuillez sélectionner aux moins une colonne.")
        }

        //Récupérer le titre de la couche histogramme
        let histogramTitle = this.shadowRoot.getElementById('histogramTitle').value;

        //validation input titre de la couche histogramme
        if(!histogramTitle){
            errorMessageList.push("Veuillez rensigner le titre la couche histogramme" );
        }

        //Récupérer la source de donnée
        let histogramDataSource = this.shadowRoot.getElementById('histogramTitle').value;

        //Récupérer la source de donnée
        let histogramDescription = this.shadowRoot.getElementById('histogramDescription').value;

        //  envoyer les message d'erreur
        if(errorMessageList.length != 0){
            let str = "<ul>";
            for (let i = 0; i < errorMessageList.length; i++) {
                str += "<li>"+ errorMessageList[i] + "</li>";
            }
            str += "</ul>";
            this.shadowRoot.getElementById('validation-message-histogram').innerHTML = str;

            return false;
        }else{
            // affectation des données
            this._histogramColones = histogramColones;
            this._histogramDescription = histogramDataSource;
            this._histogramDataSource = histogramDescription;
            this._histogramTitle = histogramTitle;
            return true;
        }

    }

    /**
     * création d'une couche histogramme
     */
    _createHistoInfo() {
        
        let validation = this._isHistogramCreationFormDataValid();

        if(validation){
            
                    //correction du titre pour l'utiliser en developpement
                    let id = workspacesService._nameFormatting(this._histogramTitle);
            
                    // ecriture des infos de la couche
                    let jsonHistogramLayer =
                        {
                            type: "histogram",
                            properties: {
                                "title": this._histogramTitle,
                                "id": id,
                                "dataSetFile": "./dataSets/"+ this.dataSetName+ "/data.json",
                                "usedData": this._histogramColones,
                                "dataSource": this._histogramDataSource,
                                "longTitle": this._histogramDescription
                            },
                            style: {
                                "fillColor": "#F2F2F2", //plusieurs couleurs de remplissage en fonction des seuils
                                "borderColor": "#FFFFF", // une seule couleur de contour
                                "display": false
                            }
                        }
            
                        this._writeLayerToWorkspaceJson(jsonHistogramLayer);

        }
        
    }

    /**
     * ecrire une couche dans le workspace.json
     * @param {*} layer couche
     */
    _writeLayerToWorkspaceJson(layer) {
        loadJson(path.resolve(workspace, this.workspaceName, "workspace.json"),
            (d) => {
                let json = (d.layers);
                json.push(layer);
                //ordonner la list selon type
                layerService.sortLayerList(json);
                let redrectionPath = "./index.html?workspaceName=" + this.workspaceName;
                fs.writeFile(path.resolve(workspace, this.workspaceName, "workspace.json"), JSON.stringify(d), function (onError) {
                    if (onError) throw onError;
                    console.log('File is created successfully.');

                    window.location.href = redrectionPath;
                });
            },
            (err, msg) => { console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`) }
        );
    }

    /**
     * renvoi des selecteur de donnée selon le type de clacul
     */
    _renderDataSelectors(){
        if(this.calculType){
            switch (this.calculType) {
                case "valeur a":
                    return this._renderList('firstNumerator','Numérateur 1');
                
                case "somme":
                    return this._renderMultiSelectionList('firstNumerator','Numérateur 1');

                case "(somme/d)%":
                    return html`
                        ${this._renderMultiSelectionList('firstNumerator','Numérateur 1')}
                        ${this._renderList('denominator','Dénominateur')}
                        `
                case "(a/b)%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1')}
                        ${this._renderList('denominator','Dénominateur')}
                        `
                case "[(a-b)/c]%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1')}
                        ${this._renderList('secondNumerator','Numérateur 2')}
                        ${this._renderList('denominator','Dénominateur')}
                    `
                case "[S1/S2]%":
                    return html`
                        ${this._renderMultiSelectionList('firstNumerator','Numérateur 1')}
                        ${this._renderMultiSelectionList('denominator','Dénominateur')}
                    `
                case "[(a-b)/S]%":
                    return html`
                        ${this._renderList('firstNumerator','Numérateur 1')}
                        ${this._renderList('secondNumerator','Numérateur 2')}
                        ${this._renderMultiSelectionList('denominator','Dénominateur')}
                    `
            }
        }
    }

    /**
     * mis à jour le type de calcul
     * @param {*} e event
     */
    _updateCalculationType(e){
        this.calculType = e.target.value;
    }

    /**
     *  gener un input de type select
     * @param {*} name  un partie du nom de l'attibut class de l'input select
     * @param {*} label le label de l'input
     */
    _renderList(name, label){
        return html`
        <div class="selectBox" >
            <label for=${name}> ${label}</label>
            <select id='${name}-select' name=${name}>
                ${this.dataSet.map(data =>
                    html`
                        <option value="${data}">${data}</option>
                    `
                )}
            </select>
        </div>
        `
    }

    /**
     * gener un input de selction multiple
     * @param {*} name  un partie de la valeur de l'attibut class de l'input select
     * @param {*} label le label de l'input
     */
    _renderMultiSelectionList(name, label){
        return html`
            <div class="selectBox" >
                <label > ${label}</label>
                <div class="dorpDown-element" @click=${()=>this._toggleListCheckBoxes(name)}>
                    <div class='text'> selectionner des colonnes </div> <fa-icon icon="angle-down" ></fa-icon>
                </div>
                <div id='${name}-dropList' class="multipleChoiceDropList " style='display:none'>
                    ${this.dataSet.map(element =>
                        html`
                            <div>
                                <input type="checkbox" name="histo-data1" class="${name}-Checkbox" value="${element}" >${element}</input>
                            </div>
                        `
                    )}
                </div>            
            </div>
        `
    }

    /**
     * afficher/cacher le MultiSelectionList
     * @param {*} name une partie de la valeur de l'attribut class de MultiSelectionList
     */
    _toggleListCheckBoxes(name){
        let dropList = this.shadowRoot.getElementById(name+'-dropList');
        let display = dropList.style.display;
        dropList.style.display = display == 'none' ? 'block': 'none'; 

    }
    
    /**
     * rajouter un input pour choisir une couleur
     * @param {*} color la couleur iniatale
     */
    _addColorPicker(color){
        if(color){
            this._iconFillColors.push(color);
        }else{
            this._iconFillColors.push('#FFFFF');
        }
        this._updateColorPickers();
    }


    /**
     * Mis à jour des selecteur du coueleur celon la liste _iconFillColors
     */
    _updateColorPickers() {
        
        //vider le div qui contient les couleurw pickers
        let iconFillColorsPickers = this.shadowRoot.getElementById('iconFillColorsPickers');
        iconFillColorsPickers.innerHTML ='';



        for (let i = 0; i < this._iconFillColors.length; i++) {
            //creation du conteneur  (input)
            let container = document.createElement('div');
            container.className = 'colorPicker';
            
            //creation du input color
            let input = document.createElement('input');
            input.type = 'color';
            input.name = 'colorPicker';
            
            //rajout d'une couleur à la liste

            input.value = this._iconFillColors[i];
            container.id = i;
            
            //Déclencher au choix d'une nouvelle couleur
            input.onchange = (e) => {
                //get index from colorPicker div
                let container = e.target.parentNode;
                let index = container.id;


                this._iconFillColors[index] = e.target.value;
            };

            let button;
            if(i == 0){
                //creation du button d'ajour d'un couleur picker
                button = document.createElement('button');
                button.type = 'button';
                button.append('+');
                button.onclick = (e)=>{   
                    this._addColorPicker();
                }
            }else{
                //creation du button du supression du couleur picker
                button = document.createElement('button');
                button.type = 'button';
                button.append('-');
                button.onclick = (e)=>{   
                    this._deleteColorPicker(e);
                }
            }
            container.append(input);
            container.append(button);
            iconFillColorsPickers.appendChild(container);
        }
    }
    
    /**
     * supprimer un couleur picker
     * @param {*} e event
     */
    _deleteColorPicker(e){
        //get index from colorPicker div
        let container = e.target.parentNode;
        let index = container.id;

        //supprimer la couleur du tableau
        this._iconFillColors.splice(index, 1);

        this._updateColorPickers();
    }

    /**
     * définier un set de couleur et de quantile 
     */
    _iconBrownFiveColor(){
        this._iconFillColors = ['#ECCF9A','#DC965A','#D67046','#B34B34','#5A1B1E'];
        this._updateColorPickers();
    }
    
    /**
     * définier un set de couleur et de quantile 
     */
    _iconRedFiveColor(){
        this._iconFillColors = ['#FFDC8C','#FFB66E','#FF8538','#F24F12','#E50916'];
        this._updateColorPickers();
    }

    /** Affichage */
    render(){
        return html`
        <div class="form">
            <h2>Formulaire de création d'une couche de données :</h2>
            
            <form>

                <label>Choisissez le type de calcul : </label>
                <div>
                    ${this.calculTypeTab.map((element, i) =>
                    html`
                        <label for="calculType">${element}</label>
                        <input type="radio" id="${i}" name="calculType" value="${element}" @change=${this._updateCalculationType}>
                        <br />
                    `)}
                </div>

                <div id="data-selection">
                        ${this._renderDataSelectors()}
                </div>

                <div>
                    <label for="layerTitle">Titre de la couche</label>
                    <input type="text" id="layerTitle" name="layerTitle" value="" placeholder="titre de la couche">
                </div>

                <div>
                    <label for="dataSource">Source des données</label>
                    <input type="text" id="dataSource" name="dataSource" value="" placeholder="CAF 69 2017">
                </div>

                <div>
                    <label for="layerDescription">Libellé long</label>
                    <input type="text" id="layerDescription" name="layerDescription" value="" placeholder="Libellé long">
                </div>
                
                <div>
                    <label> Couleur par default </label>
                    <button type="button" @click=${this._iconBrownFiveColor}>5 couleur marron</button>
                    <button type="button" @click=${this._iconRedFiveColor}>5 couleur rouge</button>
                </div>
                <div>
                    <label> Couleur et quantiles : </label>
                    <div id='iconFillColorsPickers'>
                        <!-- inputs injected dynamically -->
                    </div>
                </div>
                <div>
                    <label for="icone">Icône:</label>
                    <select id="icone" name="iconType">
                        ${ IconTypeConstants.ICON_TYPES.map( iconType => {
                            return html`<option value=${iconType}>${IconTypeConstants.iconLabel(iconType)} </option>`
                        })                        
                        }
                    </select>
                </div>


            </form>
            <div id='validation-message'></div>
            <button @click="${this._createLayerInfo}">créer couche</button>
        </div>
        <div class="form">
            <h2>Formulaire de création d'un histogramme :</h2>
            
            <form>

                <label>Choisissez les colonnes de l'histogramme : </label>


 
                ${this._renderMultiSelectionList('histogramColones')}

                <div>
                    <label for="histogramTitle">Titre de la couche histogramme</label>
                    <input type="text" id="histogramTitle" name="histogramTitle" value="" placeholder="titre de l'histogramme ">
                </div>

                <div>
                    <label for="histogramDataSource">Source des données</label>
                    <input type="text" id="histogramDataSource" name="histogramDataSource" value="" placeholder="CAF 69 2017">
                </div>

                <div>
                    <label for="histogramDescription">Libellé long histogramme</label>
                    <input type="text" id="histogramDescription" name="histogramDescription" value="" placeholder="Libellé long">
                </div>

            </form>
            <div id='validation-message-histogram'></div>
            <button @click="${this._createHistoInfo}">créer couche histogramme</button>
        </div>
        `;
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`
        :host{
            display : flex;
        }
        .form{
            width : 100%;
            min-height : 95vh;
        }
        
        .multipleChoiceDropList {
            border: 1px #dadada solid;
            height: 300px;
            overflow: scroll;
            position: absolute;
            background-color: white;
            left: 100px;
            z-index: 1;
        }

        .selectBox .dorpDown-element fa-icon{
            display: inline;

        }
        .selectBox {
            position: relative;
            margin-bottom: 5px;
            margin-top: 5px;
        }

        .selectBox label{
            display: inline;
        }
        .selectBox select{
            display: inline;
            width: 300px;
        }
        .selectBox .dorpDown-element{
            display: inline;
            border: solid;
            border-width: 1px;
            padding-left: 2px;
            padding-right: 2px;
        }

        .dorpDown-element:hover {
            background-color: #1e90ff;

        }

        .selectBox .dorpDown-element .text{
            display: inline;
        }

        #validation-message, #validation-message-histogram{
            background-color: #efcccc;
        }
        
        #data1 label:hover, #histo-data1 label {
          background-color: #1e90ff;
        }
        
        .dataCheckbox {
        width: fit-content;
        }
        `;
    }
}

customElements.define("layer-creation", LayerCreation);
export default LayerCreation;
