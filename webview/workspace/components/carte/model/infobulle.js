import InfoBulleZone from "./infobulle-zone.js";

class InfoBulle {
    /**
     * Largeur de l'info bulle
     *
     * @static
     * @memberof InfoBulle
     */
    static WIDTH = 200;

    /**
     * Padding d'affiche
     *
     * @static
     * @memberof InfoBulle
     */
    static PADDING = 5;

    /**
     * Espace entre deux zones d'affichage
     *
     * @static
     * @memberof InfoBulle
     */
    static ITEM_PADDING = 3;

    /**
     *Bordure de l'info bulle
     *
     * @static
     * @memberof InfoBulle
     */
    static BORDER = 1.5;
    /**
     *Nombre de pixel pôur l'interligne
     * 
     * @static
     * @memberof Legend
     */
    static INTERLIGNE = 2;

    /** Carte affiché */
    _carte;

    /** Liste des zones */
    _zones;

    /** Groupe associé à l'info bulle */
    _g;

    /** Rectangle définissant le pourtour de l'info bulle */
    _rect;

    /** info bulle coordinates */
    _coor = {x:0,y:0};

    _topLayerInfo;

    /**
     * Constrcuteur
     * @param {*} carte carte associé à l'info bulle 
     * @param {*} g groupe SVG contenant l'info bulle
     * @param {*} multivalue spécifie si l'info bulle doit afficher plusieurs information ou seulement celle de la couche la plus élevée
     */
    constructor(carte, g, multivalue){
        this._mutlivalue = !!multivalue;
        this._carte = carte;
        this._g = g;
        this._g.addClass("infobulle");
        this._rect = this._g.rect();
        this._rect.addClass("main");
        this._zones = new Map();
    }

    /**
     * Récupération de la largeur du texte
     *
     * @readonly
     * @memberof InfoBulle
     */
    get width(){
        return InfoBulle.WIDTH;
    }

    /**
     * Récupération de la valeur de l'interligne
     *
     * @readonly
     * @memberof InfoBulle
     */
       get interligne(){
        return InfoBulle.INTERLIGNE;
    }

    /**
     * Récupération de la valeur du padding horizontal
     *
     * @readonly
     * @memberof InfoBulle
     */
     get hPadding(){
        return InfoBulle.PADDING;
    }

    /** carte associée à la légende */
    get carte(){
        return this._carte;
    }

    /**
      * Renvoi le ratio valeur/pixel dans le svg
      *
      * @readonly
      * @memberof Legend
      */
     get ratio(){
        return this.carte.pixelRatio;
    }

    /** Définition de la tail de la fonte */
    get fontsize(){
        let stdSize = getComputedStyle(this._carte).getPropertyValue('--font-size-body3');
        return parseInt(parseInt(stdSize) * this.ratio.h);
    }    

     /**
      * Renvoi le ratio valeur/pixel dans le svg
      *
      * @readonly
      * @memberof Legend
      */
     get ratio(){
         return this._carte.pixelRatio;
     }
     /**
      * Masque les boutons d'action 
      */
     hide(){
        this._g.node.style.visibility = "hidden";
     }

     /**
     * affiche les boutons d'action 
     */
     show(){
      this._g.node.style.visibility = "visible";
     }

    /**
     * Déclaration d'une nouvelle info
     * @param {*} zone 
     * @param {*} layer 
     * @param {*} value 
     */
    addInfo(zone,layer,value){
        let infoZone = this._zones.get(zone.idCarte);
        if(!infoZone){
            infoZone = new InfoBulleZone(this, zone, this._mutlivalue);
            this._zones.set(zone.idCarte, infoZone);
        }
        infoZone.addInfo(layer, value);
        this.draw();
    }

    /**
     * Suppression d'une info
     * @param {*} zone 
     * @param {*} layer 
     * @param {*} value 
     */
    removeInfo(zone,layer,value){
        let infoZone = this._zones.get(zone.idCarte);
        if(infoZone){
            infoZone.removeInfo(layer.id);
            if(infoZone.isEmpty()){
                infoZone.remove();
                this._zones.delete(zone.idCarte);
            }
        }
        
        this.draw();
    }


   /**
     * Positionne les infos de la zone dans l'info bulle
     * @param {*} x 
     * @param {*} y 
     */
    setPosition(x,y){
        let matrix = new Snap.Matrix();
        matrix.translate(x- this.width/2 * this.ratio.w ,y);
        this._g.transform(matrix.toTransformString()); 
    }

    /**
     * Dessine la légende et l'ensemble des éléments de légende
     */
     draw(){
        if(this._zones.size === 0){
            this.hide();
        }else{
            this.show();
            let ratio = this.ratio;
            let hPadding = InfoBulle.PADDING * ratio.h;
            let top = hPadding;
            let maxX = 0;
            let maxY = 0;
            if(this._mutlivalue){
                //dessin de chaque zone
                this._zones.forEach( v => {
                    v.setPosition(hPadding,top);
                    maxX = v.getBBox().x + v.getBBox().width;
                    maxY = v.getBBox().y + v.getBBox().height + 10;
                    top = maxY + hPadding;
                });
            }else{
                let layer = { depth : Number.MAX_VALUE, value : Number.MIN_VALUE, id : null};
                let displayZone;
                this._zones.forEach( v => {
                    let topLayer = v.topLayer();
                    v.hide();
                    if(topLayer.depth < layer.depth || 
                        (topLayer.depth === layer.depth && topLayer.value < layer.value)){
                            layer = topLayer;
                            displayZone = v;
                    }
                });
                displayZone.show();
                displayZone.setPosition(hPadding,top);
                maxX = displayZone.getBBox().x + displayZone.getBBox().width;
                maxY = displayZone.getBBox().y + displayZone.getBBox().height + 10;
                top = maxY + hPadding;
            }
            
            //dessin du cadre
            this._rect.attr( {
            "x" : 0,
            "y" : 0,
            "width" : maxX + hPadding,
            "height" : maxY,
            "strokeWidth": InfoBulle.BORDER *  this.ratio.w});
        }
    }

}

export default InfoBulle;