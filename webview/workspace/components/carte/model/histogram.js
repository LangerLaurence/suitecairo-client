class Histogram {

    static WIDTH = 300;
    static HEIGHT = 300;
    static PADDING = 5;
    static BORDER = 1.5;

    static LEGEND_WIDTH = 200;
    static LEGEND_PADDING = 5;
    static LEGEND_BORDER = 1.5;
    static LEGEND_MARGIN = 10;


    static LEGEND_ICON_WIDTH = 20;
    static LEGEND_ICON_HEIGHT = 10;

    static BAR_WIDTH = 30;
    static BAR_GAP = 8;
    static BAR_MAX_HEIGHT = 200;

    static GAP_HISTOGRAM_LEGEND = 10;
    static TITLE_HEIGHT = this.fontsize;
    static MARGIN_LEGEND_TITLE = 20;

    
    /**
      * Composant de la carte
      *
      * @memberof Histogram
      */
     _carte;


    /**
      * group globale de l'histogram (histograme + legende)
      *
      * @memberof Histogram
      */
     _g

     /**
      * contiens les données à afficher dans l'histograme
      */
     _data;

     /**
      * Titre de l'histogramme
      *
      * @memberof Histogram
      */
     _title

     /**
      * Text SVG associé à l'histogramme
      *
      * @memberof Histogram
      */
     _tTitle;

    /**
     * groupe de legend de l'histogram
     */
    _gLegend;

    /**
     * group de l'histogram
     */
    _gHistogram;
    
    /**
      * 
      * @param {*} carte carte associée à la légende
      * @param {*} g groupe contenant à la légende
      */
     constructor(carte, g){
        this._carte = carte;
        this._g = g;
        this._g.addClass("histogram");
        this._initGroups();
     }

     _initGroups(){
        this._gLegend= this._g.g();
        this._gHistogram= this._g.g();
        this._tTitle = this._g.text();
     }

    /**
     * Renvoi le ratio valeur/pixel dans le svg
     *
     * @readonly
     * @memberof Histogram
     */
    get ratio() {
        return this._carte.pixelRatio;
    }

    /**
      * Masque les boutons d'action 
      */
     hide(){
        this._g.node.style.visibility = "hidden";
     }

     /**
     * affiche les boutons d'action 
     */
     show(){
      this._g.node.style.visibility = "visible";
     }


    draw() {

        if (this._data) {
            let hPadding = Histogram.PADDING * this.ratio.h;
            let wPadding = Histogram.PADDING * this.ratio.w;
            
            let nbrLegend = this._data.columns.length;
            let nbrHistogramBars = this._data.columns.length;

            // Définition des  coordonnées de titre de l'histogramme
            let histogramTitle = {
                height: this.fontsize + hPadding,
                y : nbrLegend * (this.fontsize + Histogram.LEGEND_PADDING * this.ratio.h)  + hPadding + this.fontsize,
                value: this._data.zoneLabel
            };

            // Définition des  coordonnées du conteneur des legend
            let legendContainerCoordinates = {
                x: (this._carte.width - Histogram.LEGEND_WIDTH - Histogram.LEGEND_MARGIN) * this.ratio.w,
                y: this._carte.height * this.ratio.h - (/*height of legend list*/nbrLegend * (this.fontsize + Histogram.LEGEND_PADDING * this.ratio.h)  + hPadding + this.fontsize) - histogramTitle.height,
                width: (Histogram.LEGEND_WIDTH) * this.ratio.w,
                height : ((this.fontsize + Histogram.LEGEND_PADDING * this.ratio.h) * nbrLegend) + hPadding + histogramTitle.height
            }

            // Définition des coordonnées de conteneur de l'histogramme
            let histogramContainerCoordinates = {
                x: legendContainerCoordinates.x - (  nbrHistogramBars * (Histogram.BAR_WIDTH + Histogram.BAR_GAP) + Histogram.GAP_HISTOGRAM_LEGEND) * this.ratio.w ,
                y: (this._carte.height - Histogram.BAR_MAX_HEIGHT) * this.ratio.h - (3 * hPadding),
                width : (Histogram.BAR_WIDTH + Histogram.BAR_GAP) * nbrHistogramBars  * this.ratio.w,
                height : Histogram.BAR_MAX_HEIGHT * this.ratio.h + (2 * hPadding)
            }

            //dessin de l'histogramme

            let maxValue = Math.max(...this._data.values);
            let historgramHeight = Histogram.BAR_MAX_HEIGHT - this.fontsize - Histogram.PADDING;
            let histogramBarRatio = historgramHeight / maxValue;

            for (let i = 0; i < this._data.columns.length; i++) {
                
                //dessin du labele de la legend
                let legendLabel = this._gLegend.text();
                let text = this._data.columns[i];
                if( text.length > 27 ){
                    text = text.substring(26, -1) + ' ...';
                }
                
                let yLegend =  i * (this.fontsize + Histogram.LEGEND_PADDING * this.ratio.h) +this.fontsize;
                legendLabel.attr({
                    "y": yLegend,
                    "text": text,
                    "font-size": this.fontsize,
                });
 
                 legendLabel.append(Snap.parse('<title>' + this._data.columns[i] + '</title>'));


                //dessin de l'icone de la legend

                let icon = this._gLegend.rect();

                icon.attr({
                    x: (Histogram.LEGEND_WIDTH - Histogram.LEGEND_PADDING - Histogram.LEGEND_ICON_WIDTH) * this.ratio.w - wPadding,
                    y:  yLegend - (this.fontsize/1.5),
                    width: Histogram.LEGEND_ICON_WIDTH * this.ratio.w,
                    height: Histogram.LEGEND_ICON_HEIGHT * this.ratio.h,
                    fill: this._data.colors[i]
                });


                //dessin des bars de l'histogramme

                const x = (Histogram.BAR_WIDTH + Histogram.BAR_GAP) * i * this.ratio.w;
                const y = (Histogram.BAR_MAX_HEIGHT - (this._data.values[i] * histogramBarRatio)) * this.ratio.h + hPadding;

                let bar = this._gHistogram.rect();
                bar.attr({
                    x: x,
                    y: y,
                    width: Histogram.BAR_WIDTH * this.ratio.w,
                    height: this._data.values[i] * histogramBarRatio * this.ratio.h,
                    fill: this._data.colors[i]
                });
                let barLabel = this._gHistogram.text();
                barLabel.attr({
                    x: x,
                    y: y - hPadding,
                    "text": this._data.values[i],
                    "font-size": this.fontsize
                });
                barLabel.attr("x",(bar.getBBox().width - barLabel.getBBox().width)/2 + x);
                bar.append(Snap.parse('<title>' + this._data.values[i] + '</title>'))

            }


            //titre de l'histogramme
            this._gLegend.text().attr({
                "y": histogramTitle.y,
                "text": histogramTitle.value,
                "font-size": this.fontsize,
            });

            // transformation des 2 groupes _gHistogram et _gLegend pour que les positions
            //de tous les le elements utilisant ces 2 groupes soit relatif a eux 
            let matrixLegend = new Snap.Matrix();

            matrixLegend.translate(legendContainerCoordinates.x + wPadding, legendContainerCoordinates.y);
            this._gLegend.transform(matrixLegend.toTransformString());

            let matrixHistogram = new Snap.Matrix();

            matrixHistogram.translate(histogramContainerCoordinates.x + wPadding, histogramContainerCoordinates.y);
            this._gHistogram.transform(matrixHistogram.toTransformString());

            //dessin du background du l'historgramme
            let backGround = {
                x : histogramContainerCoordinates.x,
                y : histogramContainerCoordinates.y < legendContainerCoordinates.y ? histogramContainerCoordinates.y : legendContainerCoordinates.y,
                width : histogramContainerCoordinates.width + legendContainerCoordinates.width + (Histogram.GAP_HISTOGRAM_LEGEND * this.ratio.w), 
                height : (histogramContainerCoordinates.height > legendContainerCoordinates.height ? histogramContainerCoordinates.height : legendContainerCoordinates.height ) +hPadding
            }

            let backGroundRect = this._g.rect().attr({
                x: backGround.x,
                y: backGround.y,
                width: backGround.width,
                height: backGround.height,
                fill: '#ffffff',
                id : 'histogramBackground'
            });
            backGroundRect.bringToBack();

            //Définition du titre
            this._tTitle.attr({
                y: backGround.y + this.fontsize + Histogram.PADDING,
                "text": this._title.toUpperCase(),
                "font-size": this.fontsize
            });
            this._tTitle.attr({x: backGround.x + backGround.width - this._tTitle.getBBox().width - Histogram.PADDING});
        }


    }

    /**
     * mise à jour de l'histogramme
     * @param {*} title titre de la couche associée à l'histogramme
     * @param {*} data donnée à mettre à jour avec
     */
    refresh(title, data){
        this._title = title;
        if(data){
            this._data = data;
            this.cleanHistogram();
            this.draw();
            this.show();
        }else{
            this.cleanHistogram();
            this.draw();
        }
    }

    /**
     * Effacer l'affichage des données actullees de l'histogramme et Reinialiser les group. 
     */
    cleanHistogram(){
        this._gHistogram.remove();
        this._gLegend.remove();
        this._tTitle.remove();
        let background = this._g.select('#histogramBackground');
        if (background) {
            background.remove();
        }  
        this._initGroups();
    }

    /**
     * Effacer les données actuelles et l'affichage de l'histogramme. 
     */
    eraseHistogram(){
        this.cleanHistogram();
        this._data = undefined;
    }

    /**
    * on change le masquage de la couche
    * @param {*} id 
    */
    toggle(id) {
        if (this._data) {
            if (this._data.layerId == id ) {
                if (this._g.node.style.visibility === "visible"
                    || this._g.node.style.visibility === "") {
                    this.hide();
                } else {
                    this.show();
                }
            }
        }
    }

    /**
     * Récupérer l'id de la couche utilisant l'histogramme
     */
    currentLayerId(){
        if(this._data)
            return this._data.layerId;
    }

    /** Définition de la taille de la fonte */
    get fontsize() {
    let stdSize = getComputedStyle(this._carte).getPropertyValue('--font-size-body3');
    return parseInt(parseInt(stdSize) * this.ratio.h);
 }
    
}

export default Histogram;