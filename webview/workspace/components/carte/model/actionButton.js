/**
 * Background de carte
 */
 class ActionButtons {
     /**
      * Composant de la carte
      *
      * @memberof ActionButtons
      */
     _carte;

     /**
      * group pour l'ensemble des bounton
      *
      * @memberof ActionButtons
      */
     _g

     /**
      * 
      * @param {*} carte carte associée aux boutons 
      * @param {*} g groupe contenant les oubtons
      */
     constructor(carte, g){
        this._carte = carte;
        this._g = g;
     }

     /**
      * Masque les boutons d'action 
      */
     hide(){
        this._g.node.style.visibility = "hidden";
     }

     /**
     * affiche les boutons d'action 
     */
     show(){
      this._g.node.style.visibility = "visible";
    }

     draw(){
        this.drawPlus();
        this.drawMinus();
      //   this.drawReset();
        this.drawIconPlus();
        this.drawIconMinus();       
     }
     
     drawPlus(){
        if(!this._plusPath){
            this._plusPath = this._g.path();
            this._plusPath.addClass("button");
            this._plusPath.click(this._carte.zoomIn.bind(this._carte));
        }
        let ratio = this._carte.pixelRatio;
        this._plusPath.attr("d",`M ${5 * ratio.w} ${5 * ratio.w} v ${20 * ratio.h} h ${20 * ratio.w} v ${-20 * ratio.h} h ${-20 * ratio.w} m ${10 * ratio.w} ${4 * ratio.h} v ${12 * ratio.h} m ${-6 * ratio.w} ${-6 * ratio.h} h ${12 * ratio.h}`);
        this._plusPath.attr("strokeWidth", 1.5 * ratio.w );
     }

     drawMinus(){
      if(!this._minusPath){
          this._minusPath = this._g.path();
          this._minusPath.addClass("button");
          this._minusPath.click(this._carte.zoomOut.bind(this._carte));
      }
      let ratio = this._carte.pixelRatio;
      this._minusPath.attr("d",`M ${5 * ratio.w} ${30 * ratio.w} v ${20 * ratio.h} h ${20 * ratio.w} v ${-20 * ratio.h} h ${-20 * ratio.w} m ${4 * ratio.w} ${10 * ratio.h} h ${12 * ratio.h}`);
      this._minusPath.attr("strokeWidth", 1.5 * ratio.w );
   }

   drawReset(){
      if(!this._resetPath){
          this._resetPath = this._g.path();
          this._resetPath.addClass("button");
          this._resetPath.click(this._carte.reset.bind(this._carte));
      }
      let ratio = this._carte.pixelRatio;
      this._resetPath.attr("d",`M ${5 * ratio.w} ${55 * ratio.w} v ${20 * ratio.h} h ${20 * ratio.w} v ${-20 * ratio.h} h ${-20 * ratio.w} m ${7 * ratio.w} ${7 * ratio.h} h ${6 * ratio.w} v ${6 * ratio.h} h ${-6 * ratio.w} v ${-6 * ratio.h}`);
      this._resetPath.attr("strokeWidth", 1.5 * ratio.w );
   }

    drawIconPlus() {
       if (!this.iconPlusPath) {
          this.iconPlusPath = this._g.path();
          this.iconPlusPath.addClass("button");
          this.iconPlusPath.click(this._carte.iconZoomIn.bind(this._carte));
       }
       let ratio = this._carte.pixelRatio;
       this.iconPlusPath.attr("d", `M ${5 * ratio.w} ${100 * ratio.w} v ${20 * ratio.h} h ${20 * ratio.w} v ${-20 * ratio.h} h ${-20 * ratio.w} m ${10 * ratio.w} ${4 * ratio.h} v ${12 * ratio.h} m ${-6 * ratio.w} ${-6 * ratio.h} h ${12 * ratio.h}`);
       this.iconPlusPath.attr("strokeWidth", 1.5 * ratio.w);
    }

    drawIconMinus() {
       if (!this._iconMinusPath) {
          this._iconMinusPath = this._g.path();
          this._iconMinusPath.addClass("button");
          this._iconMinusPath.click(this._carte.iconZoomOut.bind(this._carte));
       }
       let ratio = this._carte.pixelRatio;
       this._iconMinusPath.attr("d", `M ${5 * ratio.w} ${125 * ratio.w} v ${20 * ratio.h} h ${20 * ratio.w} v ${-20 * ratio.h} h ${-20 * ratio.w} m ${4 * ratio.w} ${10 * ratio.h} h ${12 * ratio.h}`);
       this._iconMinusPath.attr("strokeWidth", 1.5 * ratio.w);
    }
 }

 export default ActionButtons;