
class InfoBulleZone {

    /**
    * Info bulle contenant l'item
    *
    * @memberof InfoBulleZone
    */
    _infoBulle;

    /** Groupe SVG associé zone d'info */
    _g;

    /** Zone associé à l'info */
    _zone;

    /** liste des infos de la zone à afficher */
    _infos;

    /** Affichage de l'ensemble des zones survolée */
    _multivalue;

    /**
     * 
     * @param {*} infobulle 
     * @param {*} zone 
     */
    constructor(infobulle, zone, multivalue){
        this._multivalue = multivalue;
        this._zone = zone;
        this._infoBulle = infobulle;
        this._g = infobulle._g.g();
        this._infos = new Map();

        //création du titre
        let ratio = this._infoBulle.ratio;
        let fontSize =  this._infoBulle.fontsize;
        this._g.text(0,fontSize/2, zone.title).attr({"font-size" : fontSize + "px"}).addClass("zone-title");
    }

    /**
     * Positionne les infos de la zone dans l'info bulle
     * @param {*} x 
     * @param {*} y 
     */
    setPosition(x,y){
        let matrix = new Snap.Matrix();
        matrix.translate(x,y);
        this._g.transform(matrix.toTransformString()); 
    }

    /**
     * 
     * @returns Récupération du contour de la zone d'info bulle
     */
    getBBox(){
        return this._g.getBBox();
    }

    /**
     * Suppression de la partie graphique correspondant à la zone
     */
    remove(){
        this._g.remove();
        this._infos.clear();
    }

    /**
     * Identification de la couche de plus haut niveau
     * @returns 
     */
    topLayer(){
        let layer = { depth : Number.MAX_VALUE, value : Number.MIN_VALUE, id : null};
        this._infos.forEach((infoRow,key) =>{
            if(infoRow.depth < layer.depth){
                    layer = { depth : infoRow.depth, value : infoRow.data , id : key};
            }
        });
        return layer;
    }

    /**
     * Ajout d'une ligne d'info
     * @param {*} layer layer associé à l'info
     * @param {*} dataValue valeur de la donnée 
     */
    addInfo(layer, dataValue){
        let fontSize =  this._infoBulle.fontsize;
        let ratio = this._infoBulle.ratio;
        let top = this._multivalue 
            ?(this._infoBulle.interligne * ratio.h + fontSize) * (this._infos.size +1.5)
            : (this._infoBulle.interligne * ratio.h + fontSize) * 1.5; 

        let padding = this._infoBulle.hPadding * ratio.h;
        let value = this._g.text(0,top, dataValue).attr({"font-size" : fontSize + "px"});
        value.attr({"x" :  this._infoBulle.width * ratio.w - value.getBBox().w});

        let infoRow = { label : this._g.text(padding ,top, layer.title).attr({"font-size" : fontSize + "px"}),  
                        value : value,
                        depth : layer.depth,
                        data : dataValue
        }
        this._infos.set(layer.id, infoRow);
        this.displayTopLayerOnly();
    }

    displayTopLayerOnly(){
        if(!this._multivalue ){
            let topLayer = this.topLayer();
            this._infos.forEach( (infoRow, key) => {
                if(topLayer.id === key){
                    infoRow.value.node.style.visibility = "inherit";
                    infoRow.label.node.style.visibility = "inherit";
                }else{
                    infoRow.value.node.style.visibility = "hidden";
                    infoRow.label.node.style.visibility = "hidden";
                }
            });
        }
    }

    /**
     * Supression de l'information relative à une couche
     * @param {*} layerId 
     */
    removeInfo(layerId){
        let infoRow = this._infos.get(layerId);
        this._infos.delete(layerId);
        if(infoRow){
            infoRow.label.remove();
            infoRow.value.remove();
            this.displayTopLayerOnly();
        }
    }

    /**
     * Spécifie si l'info bulle pour la zone est vide
     * @returns true s'il n'y a plus d'info à afficher pour la zone
     */
    isEmpty(){
        return this._infos.size === 0;
    }

    /**
   * On masque la zone
   */
     hide(){
        let style = this._g.node.style;
        style.visibility = "hidden";
    }

    /**
     * On affiche la zone
     */
    show(){
        let style = this._g.node.style;
        style.visibility = "visible";
    }



}

export default InfoBulleZone;