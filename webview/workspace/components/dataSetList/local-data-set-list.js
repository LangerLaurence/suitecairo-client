import { LitElement, html, css } from '../../../lib/lit-element.js';
import dataSetService from '../../../shared/service/data-set-service.js';
import workspacesService from '../../../shared/service/workspacesService.js';

class LocalDataSetList extends LitElement {
    /**
     * liste des jeux de donnée
     */
    dataSets = [];

    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
            workspacename: { attribute: true, type: String }
        };
    }

    constructor() {
        super();
        this.workspacename = workspacesService.queryWorkspaceName();
    }
    /**
     * Déclenché lors du premier chargement
     */
    firstUpdated(changedProperties) {
        this._loadData();
    }

    /**
     * Récupère la liste des jeux de donnée en local
     */
    _loadData() {
        dataSetService.getDataSetListForLocalWorkspace(this.workspacename,
            (localDataSetsInfo)=>{
                this.dataSets = localDataSetsInfo;
                this.requestUpdate();
            },
            (err, msg) => {
                console.error(`Erreur dans le chargement du fichier de données : ${err} - ${msg}`)
            }
            );
    }
    


    /** 
     * Affichage
     */
    render() {
        return html`
            <h1>Liste des jeux de données dans l'espace de travail:</h1>
            ${this.dataSets.map((set) => 
            html`
                <div class="object-card">
                <a onclick="location.href='./layer-creation.html?workspaceName=${this.workspacename}&dataSetName=${set.name}'" > ${set.title}</a>
                </div>
            `)}

        `
    }
    static get styles()
    {
        return css`
        :host{
            display : flex;
            flex-direction: column;
        }
        .row {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
        }
        
        h1, h2, h3 {
            text-align: center;
        }
        
        .form-card {
            display : flex;
            flex-direction: column;
            width: 30%;        
            margin: 48px 0;
            padding: 16px;
        }

        .form-body {
            border: 2px solid black;
            border-radius: 5px;
            padding: 16px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
        .form-body.models, .form-body.maps, .form-body.lastworkspaces {
            border: none;
            padding-top: 0;
            max-height: 200px;
        }        
        .form-body.maps {
            max-height: 423px;
        }
        .form-body.lastworkspaces {
            max-height: 728px;
        }
        .form-body.import {
            overflow-y: unset;
        }
        
        .form-body::-webkit-scrollbar-track {
          padding: 2px 0;
          background-color: transparent;
        }
        .form-body::-webkit-scrollbar {
          width: 5px;
        }
        .form-body::-webkit-scrollbar-thumb {
            box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #404040;
            border: 1px solid #000;
        }
        
        .object-card {
            display : flex;
            flex-direction: column;     
            padding: 16px;
            border: 2px solid black;
            border-radius: 5px;
            margin-bottom : 8px;
        }
                
        input[type="text"] {
            box-sizing: border-box;
            width: 100%;
            padding: 0;
            height: 40px;
            border: 2px solid black;
            border-radius: 5px;
        }
        input[type="checkbox"] {
            display: none;
        }
        
        a {
            cursor: pointer;
        }
         a:hover {
            font-weight: bold;
         }
         a:active {
            color: #ffd700;
         }
         
        .ul {
            display: flex;
            flex-direction: column;
            justify-content: center;
            margin-top: 4px;
        }
        
        .delete:hover {
            background-color: red;
            color: #ffffff;
           
        }
        `;
    }
}

customElements.define("local-data-set-list", LocalDataSetList);
export default LocalDataSetList;
