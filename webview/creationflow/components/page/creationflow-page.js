import { LitElement, html, css } from '../../../lib/lit-element.js';

class CreationflowPage extends LitElement{
    
    /** Composant nework-space */
    _neworkspace;

    /**
     * Identification de la carte et chargement des données
     * @param {*} changedProperties proporiété modifiées 
     */
     firstUpdated(changedProperties){
        this._neworkspace = this.shadowRoot.getElementById("neworkspace");
    }

    
     /** Affichage */
    render(){
        return html`<new-work-space id="neworkspace"></new-work-space>`;
    }

    /** CSS spécifique au composant */
    static get styles() {
        return css`
        :host{
            display : block;
            width:100%;
        }
        `;
    }
}


customElements.define("creationflow-page", CreationflowPage);
export default CreationflowPage;