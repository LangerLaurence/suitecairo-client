/**
 * Récupératoin d'une regle de style par son selecteur CSS
 * @param {*} documentFragment  fragment (document ou shadowRoot)
 * @param {*} selectorText  texte selector
 * @param {*} styleSheetIndex index de la feuille de style à modifier
 */
export function cssRuleBySelectorText(documentFragment, selectorText, styleSheetIndex){
    let index = styleSheetIndex || 0;
    let rules = documentFragment.adoptedStyleSheets[index].cssRules;
    let rule; 
    for(let i=0; i <rules.length && !rule; i++){
        if(rules[i].selectorText === selectorText){
            rule = rules[i];
        }
    }
   return rule;
}

/**
 * Modification de la propriété d'une rèlge
 * @param {*} cssRule règle css
 * @param {*} propertyName nom de la propriété
 * @param {*} propertyValue valeur de la propriété
 * @param {*} priority priorité ("important", null ou vide)
 */
export function setCssProperty(cssRule, propertyName, propertyValue, priority){
    cssRule.style.setProperty(propertyName, propertyValue, priority);
}

/**
 * Conversion couleur hsl en #RRGGBB
 * @param {*} h 
 * @param {*} s 
 * @param {*} l 
 * @returns la couleur au format #RRGGBB
 */
export function hslToHex(h, s, l) {
    l /= 100;
    const a = s * Math.min(l, 1 - l) / 100;
    const f = n => {
      const k = (n + h / 30) % 12;
      const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
      // convertion en Hex et on préfixe par et on préfixe par "0" si besoin
      return Math.round(255 * color).toString(16).padStart(2, '0');   
    };
    return `#${f(0)}${f(8)}${f(4)}`;
}

/**
 * Transformation de la couleur de hex en hsl
 * @param {*} H couleur au format #RRGGBB
 * @returns object hsl : {h:h,s:s,l:l}
 */
export function hexToHsl(H) {
    // Convert hex to RGB first
    let r = 0, g = 0, b = 0;
    if (H.length == 4) {
      r = "0x" + H[1] + H[1];
      g = "0x" + H[2] + H[2];
      b = "0x" + H[3] + H[3];
    } else if (H.length == 7) {
      r = "0x" + H[1] + H[2];
      g = "0x" + H[3] + H[4];
      b = "0x" + H[5] + H[6];
    }
    // Then to HSL
    r /= 255;
    g /= 255;
    b /= 255;
    let cmin = Math.min(r,g,b),
        cmax = Math.max(r,g,b),
        delta = cmax - cmin,
        h = 0,
        s = 0,
        l = 0;
  
    if (delta == 0)
      h = 0;
    else if (cmax == r)
      h = ((g - b) / delta) % 6;
    else if (cmax == g)
      h = (b - r) / delta + 2;
    else
      h = (r - g) / delta + 4;
  
    h = Math.round(h * 60);
  
    if (h < 0)
      h += 360;
  
    l = (cmax + cmin) / 2;
    s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
    s = +(s * 100).toFixed(1);
    l = +(l * 100).toFixed(1);
  
    return {h,s,l};
  }