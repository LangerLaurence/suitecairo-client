const fs = require('fs');

/**
 * 
 * @param {Lecture asynchrone d'un fichier JSON} path 
 * @param {*} onSuccess call back de succes prend en paramètre le json lu
 * @param {*} onError clallback d'erreur prend en paramètre : le nom de l'erreur et le message de l'erreur
 */
export function loadJson(path, onSuccess, onError){
    fs.readFile(path, 'utf8', (err, data) => {
        if (err) {
           onError(err.name, err.message);
        }else{
            let res;
            try{
                res =JSON.parse(data);
            }catch(e){
                onError(e.name, e.message);
            }
            onSuccess(res);
        }
    });
}