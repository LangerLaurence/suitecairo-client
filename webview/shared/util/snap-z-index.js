
/** Plugin snap permettant de gérer l'ordre d'empillement des éléments svg*/
Snap.plugin(function(Snap, Element, Paper, global) {
    const elProto = Element.prototype;
  
    /** Amène l'élément en haut de la pile d'affichage */
    elProto.bringToFront = function() {
      const parent = this.node.parentNode;
      parent.appendChild(this.node);
      return this;
    };
  
     /** Amène l'élément en bas de la pile d'affichage */
    elProto.bringToBack = function() {
      const parent = this.node.parentNode;
      parent.insertBefore(this.node, parent.firstElementChild);
      return this;
    };
    
    /**
     * Descend l'élément d'un cran dans la pile d'affichage
     */
    elProto.decZindex = function() {
      const domElement = this.node;
      const prevElement = domElement.previousElementSibling;
      if(prevElement) {
        domElement.parentNode.insertBefore(domElement, prevElement);
      }
      return this;
    }
  
    /**
     * Monte l'élément d'un cran dans la pile d'affichage
     */
    elProto.incZindex = function() {
      const domElement = this.node;
      const nextElement = domElement.nextElementSibling;
      if(nextElement) {
        domElement.parentNode.insertBefore(domElement, nextElement.nextElementSibling);
      }
      return this;
    }
  
});