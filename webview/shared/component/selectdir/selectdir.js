import { LitElement, html, css } from '../../../lib/lit-element.js';
const electron = window.require('electron');
const app = window.require('@electron/remote'); 
const dialog = app.dialog;

class SelectDir extends LitElement{
    /**
     * Propriétés et attribut de l'élément
     */
    static get properties() {
        return {
          title: {attribute: true, type: String},
          folder: {attribute: true, type: String}
        };
      }

    constructor() {
        super();
        this.title = 'Sélectionnez un répertoire';
        
    }

    displayFolder(){
        return this.folder ? this.folder : "Aucun répertoire selectionné";
    }
    /**
     * 
     * @param {*} e evènement
     */
    _handleClick(e){

        let pathPromise = dialog.showOpenDialog({
            properties: ['openDirectory']
        });
        pathPromise.then(
            (f) => { 
                if(!f.canceled){
                    this.folder = f.filePaths[0];
                    let event = new CustomEvent('folder-change', { detail: { newValue : this.folder}});
                    this.dispatchEvent(event);
                }
            },
            (e) => { console.error(e);}
        );
    }

    render(){
        return html ` ${this.displayFolder()} <button @click="${this._handleClick}">${this.title}</button>`;
    }
}

customElements.define("select-directory", SelectDir);
export default SelectDir;