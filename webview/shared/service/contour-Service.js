import HttpClient from '../util/client.js';
import layerService from "./layerService.js";
const path = require('path');
const { env } = require('../../container/environments.js');


class ContoursService extends HttpClient{

/**
     * Récuperation de la liste des contours qui correspond à une carte
     * @param {*} echelle echelle de la carte
     * @param {*} maille maille de la carte
     * @param {*} onSuccess 
     * @param {*} onError 
     */
 getContoursList(echelle, maille, onSuccess, onError){
    let url = this.url("api/contours")
    let params = "?echelle=" + echelle + "&maille=" + maille;
    this.getJson(url+params, onSuccess, onError);
}

/**
     * téléchargement et extraction  du zip du contour depuis le serveur
     * @param {*} contourName Nom du contour
     * @param {*} workspaceName Nom de l'espace du travail
     * @param {*} onSuccess 
     * @param {*} onError 
     */
 getContourZip(contourName, workspaceName, onSuccess, onError) {
    let url = "api/contours/" + contourName;
    let destination = path.resolve(env.get("workspace.location"), workspaceName, "edgelayer", contourName);
    this.downloadZipFile(url, destination, onSuccess, onError);
}

}

export default new ContoursService();
